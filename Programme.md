# Programme

Le grand principe : une première partie non technique, la suite est orientée technique et pratique.

## Nos a-prioris sur le Logiciel Libre

* Quels logiciels libres utilisez-vous déjà ?
* Quels sont les reproches formulés à l'encontre du logiciel libre (à tort ou à raison)
* Qu'est-ce qui ne fonctionne pas dans le logiciel libre

## Travail collaboratif avec Git & Gitlab (mode web)

Objectif :

* Considérons ce projet https://gitlab.adullact.net/adullact/Accompagnement-git-gitlab
* Ajouter ses prénom, nom, organisme au fichier [CONTRIBUTORS.md](https://gitlab.adullact.net/adullact/Accompagnement-git-gitlab/blob/master/CONTRIBUTORS.md) 

Contraintes :

* Nous ne faisons pas parti du projet, nous sommes des contributeurs extérieurs

Conséquences :

* Besoin de passer par un fork personnel (préciser les deux sens de fork) dans son environnement Gitlab 
* Faire la modification
* Proposer la Merge Request
* Attendre validation
* Gérer d'éventuels conflits (auquel cas => ligne de commande)

Poser le flux de travail d'une contribution, et s'appuyer le [Cycle de vie d'une contribution dans Départements & Notaires](https://gitlab.adullact.net/departements-notaires/departements-notaires/blob/master/documentation/cycle-de-vie-contribution.md) 

Schéma à faire construire :

* dépôt canonique du projet
* fork perso du projet
* nom de l'action / des actions sur la flêche allant du dépôt canonique au dépôt perso
* idem mais sens contraire 

## Travail collaboratif avec Git & Gitlab (mode ligne de commande)

Schéma : 

* Compléter en introduisant la notion de local / remote

Intérêt de la ligne de commande :

* permet de gérer simplement les conflits
* permet d'utiliser toute la puissance de Git

Objectif :

* Faire le même exercice que précédemment mais avec un conflit (deux personnes font des modifications "contraires" sur une même ligne)
